package Nekhoroshev.Homework3.pack;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        IntegerArrayGenerator generator = new IntegerArrayGenerator(100);
        Integer[] result = generator.generateArray(25, true);
        MathBox box = new MathBox(result);

        System.out.println("fist box values");
        System.out.println(box.toString());

        System.out.println("All Elements sum = " + box.summator());
        List<Double> devidedCollection = box.splitter(2.0);
        for (int i = 0; i < devidedCollection.size(); i++) {
            System.out.println("divided value [" + i + "] = " + devidedCollection.get(i));
        }

        MathBox box2 = new MathBox(result);

        System.out.println("second box values");
        System.out.println(box2.toString());

        System.out.println("first box hash = " + box.hashCode() + " and second box hash = " + box2.hashCode());
        System.out.println("box.equals(box2) = " + box.equals(box2));

        if (box.remove(5)) {
            System.out.println("fist box values after remove of integer value");
            System.out.println(box.toString());

            System.out.println("first box hash = " + box.hashCode() + " and second box hash = " + box2.hashCode());
            System.out.println("box.equals(box2) = " + box.equals(box2));
        }

    }
}
