package Nekhoroshev.Homework3.pack;

import java.util.Random;

public class IntegerArrayGenerator {

    private final int upperBound;

    /**
     * Constructor
     *
     * @param upperBound - upper bound of Integer value to generate, bottom bound is 0
     */
    public IntegerArrayGenerator(int upperBound) {
        assert (upperBound > 0);
        this.upperBound = upperBound;
    }

    /**
     * method generates value from 0 to upperBound
     *
     * @return Integer value
     */
    private Integer generateValue() {
        Random random = new Random();
        return random.nextInt(upperBound);
    }

    /**
     * method generates value from 0 to 100 which is not added in array
     *
     * @param sourceArray - array for check that we does not generated a duplicate value
     * @return value, which is absent in array
     */
    private Integer generateUniqueValue(Integer[] sourceArray) {
        Integer result;
        boolean isArrayHasSuchValue;
        do {
            result = generateValue();
            isArrayHasSuchValue = false;
            for (int i = 0; i < sourceArray.length; i++) {
                if (result.equals(sourceArray[i])) {
                    isArrayHasSuchValue = true;
                }
            }
        } while (isArrayHasSuchValue);
        return result;
    }

    /**
     * meythod generates array of Integer elements in range from 0 to 100, but
     * if user wants more UNIQUE elements from array than are in range from 0 to upperBound than method returns
     * array with length as elementsCount, but not last elements values will be null
     *
     * @param elementsCount        - size of generated array
     * @param isUniqueElementsOnly - allow duplicated elements or not
     * @return returns Integer[] with size of elementsCount
     */
    public Integer[] generateArray(int elementsCount, boolean isUniqueElementsOnly) {
        Integer[] result = new Integer[elementsCount];

        for (int i = 0; i < elementsCount; i++) {
            Integer generatedValue;
            if (isUniqueElementsOnly) {
                if (i >= upperBound) {
                    break;
                }
                generatedValue = generateUniqueValue(result);

            } else {
                generatedValue = generateValue();
            }
            result[i] = generatedValue;

        }
        return result;
    }
}
