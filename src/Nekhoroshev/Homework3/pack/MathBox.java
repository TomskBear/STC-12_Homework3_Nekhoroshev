package Nekhoroshev.Homework3.pack;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.TreeSet;

public class MathBox {
    private TreeSet<Integer> internalList;

    public MathBox(Integer[] sourceArray) {
        if (sourceArray != null) {
            init(sourceArray);
        }

    }

    private void init(Integer[] sourceArray) {
        internalList = new TreeSet<>();
        for (int i = 0; i < sourceArray.length; i++) {
            internalList.add(sourceArray[i]);
        }
    }

    public Integer summator() {
        Integer result = 0;
        for (Integer listItem : internalList) {
            result += listItem;
        }
        return result;
    }

    public List<Double> splitter(Double divider) {
        List<Double> result = new ArrayList<>();
        for (Integer listItem : internalList) {
            result.add(listItem / divider);
        }
        return result;
    }

    public boolean remove(Integer value) {
        if (internalList.contains(value)) {
            internalList.remove(value);
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathBox mathBox = (MathBox) o;
        return Objects.equals(internalList, mathBox.internalList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(internalList);
    }

    @Override
    public String toString() {
        return "MathBox{" +
                "internalList=" + internalList +
                '}';
    }
}
